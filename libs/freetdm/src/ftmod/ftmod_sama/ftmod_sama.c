/*
 * Copyright (c) 2007-2014, Anthony Minessale II
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * * Neither the name of the original author; nor the names of any contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Contributors: 
 *
 * Moises Silva <moy@sangoma.com>
 * W McRoberts <fs@whmcr.com>
 * Puskás Zsolt <errotan@gmail.com>
 *
 */

#include "private/ftdm_core.h"
#include "ftmod_sama.h"

/* used by dahdi to indicate there is no data available, but events to read */
#ifndef ELAST
#define ELAST 500
#endif

/**
 * \brief Zaptel globals
 */
static struct {
	uint32_t codec_ms;
	uint32_t wink_ms;
	uint32_t flash_ms;
	uint32_t eclevel;
	uint32_t etlevel;
    struct sockaddr_in signaling_host;
    struct sockaddr_in data_host;
    float rxgain;
    float txgain;
} sama_globals;

#if defined(__FreeBSD__)
typedef unsigned long ioctlcmd;
#else
typedef int ioctlcmd;
#endif

/**
 * \brief General IOCTL codes
 */
struct ioctl_codes {
    ioctlcmd GET_BLOCKSIZE;
    ioctlcmd SET_BLOCKSIZE;
    ioctlcmd FLUSH;
    ioctlcmd SYNC;
    ioctlcmd GET_PARAMS;
    ioctlcmd SET_PARAMS;
    ioctlcmd HOOK;
    ioctlcmd GETEVENT;
    ioctlcmd IOMUX;
    ioctlcmd SPANSTAT;
    ioctlcmd MAINT;
    ioctlcmd GETCONF;
    ioctlcmd SETCONF;
    ioctlcmd CONFLINK;
    ioctlcmd CONFDIAG;
    ioctlcmd GETGAINS;
    ioctlcmd SETGAINS;
    ioctlcmd SPANCONFIG;
    ioctlcmd CHANCONFIG;
    ioctlcmd SET_BUFINFO;
    ioctlcmd GET_BUFINFO;
    ioctlcmd AUDIOMODE;
    ioctlcmd ECHOCANCEL;
    ioctlcmd HDLCRAWMODE;
    ioctlcmd HDLCFCSMODE;
    ioctlcmd SPECIFY;
    ioctlcmd SETLAW;
    ioctlcmd SETLINEAR;
    ioctlcmd GETCONFMUTE;
    ioctlcmd ECHOTRAIN;
    ioctlcmd SETTXBITS;
    ioctlcmd GETRXBITS;
    ioctlcmd TONEDETECT;
};

/**
 * \brief Sama IOCTL codes
 */
static struct ioctl_codes sama_ioctl_codes = {
    .GET_BLOCKSIZE = SAMA_GET_BLOCKSIZE,
    .SET_BLOCKSIZE = SAMA_SET_BLOCKSIZE,
    .FLUSH = SAMA_FLUSH,
    .SYNC = SAMA_SYNC,
    .GET_PARAMS = SAMA_GET_PARAMS,
    .SET_PARAMS = SAMA_SET_PARAMS,
    .HOOK = SAMA_HOOK,
    .GETEVENT = SAMA_GETEVENT,
    .IOMUX = SAMA_IOMUX,
    .SPANSTAT = SAMA_SPANSTAT,
    .MAINT = SAMA_MAINT,
    .GETCONF = SAMA_GETCONF,
    .SETCONF = SAMA_SETCONF,
    .CONFLINK = SAMA_CONFLINK,
    .CONFDIAG = SAMA_CONFDIAG,
    .GETGAINS = SAMA_GETGAINS,
    .SETGAINS = SAMA_SETGAINS,
    .SPANCONFIG = SAMA_SPANCONFIG,
    .CHANCONFIG = SAMA_CHANCONFIG,
    .SET_BUFINFO = SAMA_SET_BUFINFO,
    .GET_BUFINFO = SAMA_GET_BUFINFO,
    .AUDIOMODE = SAMA_AUDIOMODE,
    .ECHOCANCEL = SAMA_ECHOCANCEL,
    .HDLCRAWMODE = SAMA_HDLCRAWMODE,
    .HDLCFCSMODE = SAMA_HDLCFCSMODE,
    .SPECIFY = SAMA_SPECIFY,
    .SETLAW = SAMA_SETLAW,
    .SETLINEAR = SAMA_SETLINEAR,
    .GETCONFMUTE = SAMA_GETCONFMUTE,
    .ECHOTRAIN = SAMA_ECHOTRAIN,
    .SETTXBITS = SAMA_SETTXBITS,
    .GETRXBITS = SAMA_GETRXBITS,
    .TONEDETECT = SAMA_TONEDETECT,
};

#define SAMA_INVALID_SOCKET -1
#define SAMA_INVALID_EVENT -100
#define SAMA_INVALID_DATA -100
#define POLL_DATA_SIZE 100
static struct ioctl_codes codes;

static ftdm_socket_t CONTROL_FD = SAMA_INVALID_SOCKET;
static ftdm_socket_t DATA_FD = SAMA_INVALID_SOCKET;

FIO_SPAN_NEXT_EVENT_FUNCTION(sama_next_event);
FIO_SPAN_POLL_EVENT_FUNCTION(sama_poll_event);
FIO_CHANNEL_NEXT_EVENT_FUNCTION(sama_channel_next_event);

static int sama_ioctl_write(ftdm_socket_t sockfd, char *buff) {
    char temp_buff[1000];
    bzero(temp_buff, sizeof(temp_buff));
    memcpy(temp_buff, buff, strlen(buff));

    sprintf(buff, "ioctl:%s", temp_buff);
    ftdm_log(FTDM_LOG_INFO, "%s\n", buff);

    int err = 0;
    int32_t data_size = htonl(strlen(buff));
    if ((err = write(sockfd, (char*)&data_size, sizeof(data_size)) < 0 ? -1 : 0) == 0) {
        return write(sockfd, buff, strlen(buff)) < 0 ? -1 : 0;
    } else {
        return err;
    }
}

static int32_t pop_sama_data(sama_span_param_t *sama_span_params) {
    int32_t value;
    ftdm_mutex_lock(sama_span_params->ioctl_mutex);

    if (sama_span_params->ioctl_data[0] == SAMA_INVALID_DATA) {
        ftdm_mutex_unlock(sama_span_params->ioctl_mutex);
        return SAMA_INVALID_DATA;
    }

    value = sama_span_params->ioctl_data[0];
    for (int i = 0; i < MAX_QUEUE_SIZE - 1; i++) {
        sama_span_params->ioctl_data[i] = sama_span_params->ioctl_data[i + 1];
    }
    sama_span_params->ioctl_data[MAX_QUEUE_SIZE - 1] = SAMA_INVALID_DATA;

    ftdm_mutex_unlock(sama_span_params->ioctl_mutex);
    return value;
}

static int sama_ioctl(ftdm_socket_t sockfd, ioctlcmd cmd, int *data) {
    char buff[1000];
    int err;

    ftdm_log(FTDM_LOG_INFO, "sama ioctl. cmd: %d, data: %d\n", cmd, *data);
    bzero(buff, sizeof(buff));

    switch (cmd) {
    case SAMA_GET_BLOCKSIZE:
    case SAMA_GETEVENT:
        sprintf(buff, "%d,%d", cmd, 0);

        err = sama_ioctl_write(sockfd, buff);
        ftdm_log(FTDM_LOG_INFO, "err: %d\n", err);
        err = err < 0 ? -1 : 0;
        if (err) {
            ftdm_log(FTDM_LOG_ERROR, "Can't write in socket.\n");
        } else {
            while (1) {
                *data = pop_sama_data(sama_span_params);
                if (*data == SAMA_INVALID_DATA) {
                    continue;
                } else {
                    ftdm_log(FTDM_LOG_ERROR, "Read data is: %d.\n", *data);
                    break;
                }
            }
        }
        break;
    default:
        sprintf(buff, "%d,%d", cmd, *data);
        err = sama_ioctl_write(sockfd, buff);
        break;
    }
    return err;
}

static int push_sama_event(int32_t *queue, ftdm_mutex_t *mutex, int32_t *event) {
    ftdm_mutex_lock(mutex);

    for (int i = 0; i < MAX_QUEUE_SIZE; i++) {
        if (queue[i] == SAMA_INVALID_EVENT) {
            queue[i] = *event;
            ftdm_mutex_unlock(mutex);
            return 0;
        }
    }

    ftdm_mutex_unlock(mutex);
    ftdm_log(FTDM_LOG_CRIT, "Event buffer is full!.\n");
    return -1;
}

static int push_sama_value(sama_span_param_t *sama_span_params, int32_t *value) {
    ftdm_mutex_lock(sama_span_params->ioctl_mutex);

    for (int i = 0; i < MAX_QUEUE_SIZE; i++) {
        if (sama_span_params->ioctl_data[i] == SAMA_INVALID_DATA) {
            sama_span_params->ioctl_data[i] = *value;
            ftdm_mutex_unlock(sama_span_params->ioctl_mutex);
            return 0;
        }
    }

    ftdm_mutex_unlock(sama_span_params->ioctl_mutex);
    ftdm_log(FTDM_LOG_CRIT, "ioctl buffer is full!.\n");
    return -1;
}

static int32_t pop_event(int32_t *queue, ftdm_mutex_t *mutex) {
    int32_t event;
    ftdm_mutex_lock(mutex);

    if (queue[0] == SAMA_INVALID_EVENT) {
        ftdm_mutex_unlock(mutex);
        return -2;
    }

    event = queue[0];
    for (int i = 0; i < MAX_QUEUE_SIZE - 1; i++) {
        queue[i] = queue[i + 1];
    }
    queue[MAX_QUEUE_SIZE - 1] = SAMA_INVALID_EVENT;

    ftdm_mutex_unlock(mutex);
    return event;
}

static int32_t pop_sama_event(sama_span_param_t *sama_span_params, int32_t event) {
    if (event == 1) {
        return pop_event(sama_span_params->in_event_queue, sama_span_params->in_event_mutex);
    } else if (event == 2) {
        return pop_event(sama_span_params->pri_event_queue, sama_span_params->pri_event_mutex);
    } else if (event == 4) {
        return pop_event(sama_span_params->out_event_queue, sama_span_params->out_event_mutex);
    }
    return -2;
}

static int32_t queue_has_event(int32_t *queue, ftdm_mutex_t *mutex) {
    ftdm_mutex_lock(mutex);

    if (queue[0] == SAMA_INVALID_EVENT) {
        ftdm_mutex_unlock(mutex);
        return -1;
    }

    ftdm_mutex_unlock(mutex);
    return 0;
}

static int32_t sama_has_event(sama_span_param_t *sama_span_params, int32_t event) {
    if (event == 1) {
        return queue_has_event(sama_span_params->in_event_queue, sama_span_params->in_event_mutex);
    } else if (event == 2) {
        return queue_has_event(sama_span_params->pri_event_queue, sama_span_params->pri_event_mutex);
    } else if (event == 4) {
        return queue_has_event(sama_span_params->out_event_queue, sama_span_params->out_event_mutex);
    }
    return -1;
}

//static int32_t is_valid_event(sama_span_param_t *sama_span_params, int32_t *event) {
//    ftdm_mutex_lock(sama_span_params->event_mutex);

//    if (sama_span_params->event_queue[0] & *event)
//        ftdm_mutex_unlock(sama_span_params->event_mutex);
//        return 0;

//    ftdm_mutex_unlock(sama_span_params->event_mutex);
//    return -1;
//}


static int read_data(int sockfd, char *buff) {
    int32_t data_size = 0;
    int r;

    if (!sockfd) {
        return -1;
    }
    ftdm_log(FTDM_LOG_INFO, "Socket is: %d\n", sockfd);
    r = read(sockfd, (char*)&data_size, sizeof(data_size));
    if (r <= 0) {
        return -1;
    }

    data_size = ntohl(data_size);
    ftdm_log(FTDM_LOG_INFO, "Data size is: %d\n", data_size);

    if (data_size > 100) {
        ftdm_log(FTDM_LOG_ERROR, "Data size is invalid! %d\n", ntohl(data_size));
        return -1;
    }

    bzero(buff, POLL_DATA_SIZE);
    r = read(sockfd, buff, data_size);
    if (r <= 0) {
        ftdm_log(FTDM_LOG_ERROR, "Error in poll.\n");
        return -1;
    }
    buff[data_size] = '\0';
    ftdm_log(FTDM_LOG_INFO, "Received data is: %s\n", buff);
    return 1;
}

static int get_event(char *buff, int32_t *event) {
    char command_str[POLL_DATA_SIZE];

    if (strstr(buff, "event:") != NULL) {
        memcpy(command_str, &buff[6], POLL_DATA_SIZE - 6);
        ftdm_log(FTDM_LOG_INFO, "Received event is: %s\n", buff);
        *event = atoi(command_str);
        ftdm_log(FTDM_LOG_INFO, "Received event is: %d\n", *event);
        return 1;
    }
    return 0;
}

static int get_value(char *buff, int32_t *value) {
    char command_str[POLL_DATA_SIZE];

    if (strstr(buff, "data:") != NULL) {
        memcpy(command_str, &buff[5], POLL_DATA_SIZE - 5);
        ftdm_log(FTDM_LOG_INFO, "Received data is: %s\n", command_str);
        *value = atoi(command_str);
        ftdm_log(FTDM_LOG_INFO, "Received data is: %d\n", *value);
        return 1;
    }
    return 0;
}


static void *sama_poll(ftdm_thread_t *me, void *obj) {
    int *channo = (int *) obj;
    int r;
    int failCount = 0;
    char buff[POLL_DATA_SIZE];
    int32_t event, value;
    ftdm_unused_arg(me);

    while (1) {
        ftdm_log(FTDM_LOG_INFO, "Wait for fd simulated event!\n");
        r = read_data(CONTROL_FD, buff);
        ftdm_log(FTDM_LOG_INFO, "Data read from control socket! %s\n", buff);
        if (r <= 0) {
            failCount++;
            if (failCount > 5) {
                failCount = 0;
                return NULL;
            }
            continue;
        } else {
            failCount = 0;
        }

        if (get_event(buff, &event)) {
            ftdm_log(FTDM_LOG_INFO, "Read event output: %d and event is %d!\n", r, event);
            if (event == 1) {
                push_sama_event(sama_span_params[*channo - 1].in_event_queue, sama_span_params[*channo - 1].in_event_mutex, &event);
            } else if (event == 2) {
                push_sama_event(sama_span_params[*channo - 1].pri_event_queue, sama_span_params[*channo - 1].pri_event_mutex, &event);
            } else if (event == 4) {
                push_sama_event(sama_span_params[*channo - 1].out_event_queue, sama_span_params[*channo - 1].out_event_mutex, &event);
            }
        } else if (get_value(buff, &value)) {
            ftdm_log(FTDM_LOG_INFO, "Read value output: %d and value is %d!\n", r, value);
            push_sama_value(&sama_span_params[*channo - 1], &value);
        }
    }
    return NULL;
}

static void *accept_connection(ftdm_thread_t *me, void *obj) {
    sama_socket_connection_t *scp = (sama_socket_connection_t *) obj;
    int addrlen = sizeof(scp->address);
    ftdm_socket_t new_socket = SAMA_INVALID_SOCKET;

    ftdm_unused_arg(me);
    while (1) {
        ftdm_log(FTDM_LOG_INFO, "Wait for client!\n");
        new_socket = accept(scp->c_sockfd, (struct sockaddr *)&(scp->address), (socklen_t*)&addrlen);
        usleep(1 * 1000 * 1000);
        if (new_socket == SAMA_INVALID_SOCKET)
        {
            ftdm_log(FTDM_LOG_CRIT, "Server Failed to accept client!\n");
        } else {
            scp->o_sockfd = new_socket;
            if (scp->is_signal == 0) {
                scp->channel->sockfd = scp->o_sockfd;
            }
            if (scp->is_signal) {
                CONTROL_FD = new_socket;
                sama_ioctl(scp->o_sockfd, codes.SPECIFY, scp->chan_no);
                ftdm_thread_create_detached(sama_poll, scp->chan_no);
            }
            ftdm_log(FTDM_LOG_INFO, "Client accepted!\n");
        }
    }
    return NULL;
}

/**
 * \brief Run socket
 */
static void sama_run_sockets(struct sockaddr_in address, ftdm_socket_t *sockfd) {
    ftdm_socket_t local_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (local_socket == SAMA_INVALID_SOCKET)
    {
        ftdm_log(FTDM_LOG_CRIT, "Can't create socket file!\n");
        return;
    }

    // Binding newly created socket to given IP and verification
    if (bind(local_socket, (struct sockaddr *)&address, sizeof(address)) != 0)
    {
        ftdm_log(FTDM_LOG_CRIT, "Can't attache stocket to the port!\n");
        return;
    }

    // Now server is ready to listen and verification
    if (listen(local_socket, 5) != 0)
    {
        ftdm_log(FTDM_LOG_CRIT, "Listen failed!\n");
        return;
    }

    ftdm_log(FTDM_LOG_INFO, "Open socket is %d.\n", local_socket);
    *sockfd = local_socket;
}

/**
 * \brief Initialises codec, and rx/tx gains
 * \param g Structure for gains to be initialised
 * \param rxgain RX gain value
 * \param txgain TX gain value
 * \param codec Codec
 */
static void sama_build_gains(struct sama_gains *g, float rxgain, float txgain, int codec)
{
	int j;
	int k;
	float linear_rxgain = pow(10.0, rxgain / 20.0);
    float linear_txgain = pow(10.0, txgain / 20.0);

	switch (codec) {
	case FTDM_CODEC_ALAW:
		for (j = 0; j < (sizeof(g->receive_gain) / sizeof(g->receive_gain[0])); j++) {
			if (rxgain) {
				k = (int) (((float) alaw_to_linear(j)) * linear_rxgain);
				if (k > 32767) k = 32767;
				if (k < -32767) k = -32767;
				g->receive_gain[j] = linear_to_alaw(k);
			} else {
				g->receive_gain[j] = j;
			}
			if (txgain) {
				k = (int) (((float) alaw_to_linear(j)) * linear_txgain);
				if (k > 32767) k = 32767;
				if (k < -32767) k = -32767;
				g->transmit_gain[j] = linear_to_alaw(k);
			} else {
				g->transmit_gain[j] = j;
			}
		}
		break;
	case FTDM_CODEC_ULAW:
		for (j = 0; j < (sizeof(g->receive_gain) / sizeof(g->receive_gain[0])); j++) {
			if (rxgain) {
				k = (int) (((float) ulaw_to_linear(j)) * linear_rxgain);
				if (k > 32767) k = 32767;
				if (k < -32767) k = -32767;
				g->receive_gain[j] = linear_to_ulaw(k);
			} else {
				g->receive_gain[j] = j;
			}
			if (txgain) {
				k = (int) (((float) ulaw_to_linear(j)) * linear_txgain);
				if (k > 32767) k = 32767;
				if (k < -32767) k = -32767;
				g->transmit_gain[j] = linear_to_ulaw(k);
			} else {
				g->transmit_gain[j] = j;
			}
		}
		break;
	}
}

/**
 * \brief Initialises a range of Zaptel/DAHDI channels
 * \param span FreeTDM span
 * \param start Initial wanpipe channel number
 * \param end Final wanpipe channel number
 * \param type FreeTDM channel type
 * \param name FreeTDM span name
 * \param number FreeTDM span number
 * \param cas_bits CAS bits
 * \return number of spans configured
 */
static unsigned sama_open_range(ftdm_span_t *span, unsigned start, unsigned end, ftdm_chan_type_t type, char *name, char *number, unsigned char cas_bits)
{
    unsigned x;
    sama_params_t samap;

//   FIXME: range not suported
    memset(&samap, 0, sizeof(samap));
    sama_span_params = malloc((end - start) * sizeof(sama_span_param_t));

    for (int j = 0; j < (end - start); j++) {
        ftdm_mutex_create(&sama_span_params[j].pri_event_mutex);
        ftdm_mutex_create(&sama_span_params[j].in_event_mutex);
        ftdm_mutex_create(&sama_span_params[j].out_event_mutex);
        ftdm_mutex_create(&sama_span_params[j].ioctl_mutex);
        for (int i = 0; i < MAX_QUEUE_SIZE; i++) {
            sama_span_params[j].pri_event_queue[i] = SAMA_INVALID_EVENT;
            sama_span_params[j].in_event_queue[i] = SAMA_INVALID_EVENT;
            sama_span_params[j].out_event_queue[i] = SAMA_INVALID_EVENT;
            sama_span_params[j].ioctl_data[i] = SAMA_INVALID_DATA;
        }
    }

	for(x = start; x < end; x++) {
        sama_span_params[x - start].chan_no = x;

        sama_span_params[x - start].signaling.address = sama_globals.signaling_host;
        sama_span_params[x - start].signaling.c_sockfd = CONTROL_FD;
        sama_span_params[x - start].signaling.is_signal = 1;
        sama_span_params[x - start].signaling.chan_no = &(sama_span_params[x - start].chan_no);

        sama_span_params[x - start].data.address = sama_globals.data_host;
        sama_span_params[x - start].data.c_sockfd = DATA_FD;
        sama_span_params[x - start].data.is_signal = 0;
        sama_span_params[x - start].data.chan_no = &(sama_span_params[x - start].chan_no);

        ftdm_thread_create_detached(accept_connection, &sama_span_params[x - start].signaling);
        ftdm_thread_create_detached(accept_connection, &sama_span_params[x - start].data);

        ftdm_span_add_channel(span, FTDM_INVALID_SOCKET, type, &(sama_span_params[x - start].data.channel));

        ftdm_channel_t *ftdmchan = sama_span_params[x - start].data.channel;
        ftdmchan->rate = 8000;
        ftdmchan->physical_span_id = span->span_id;
        ftdmchan->physical_chan_id = x;


        if (type == FTDM_CHAN_TYPE_FXS || type == FTDM_CHAN_TYPE_FXO) {
            struct sama_chanconfig cc;
            memset(&cc, 0, sizeof(cc));
            cc.chan = cc.master = x;

            switch(type) {
            case FTDM_CHAN_TYPE_FXS:
                {
                    switch(span->start_type) {
                    case FTDM_ANALOG_START_KEWL:
                        cc.sigtype = ZT_SIG_FXOKS;
                        break;
                    case FTDM_ANALOG_START_LOOP:
                        cc.sigtype = ZT_SIG_FXOLS;
                        break;
                    case FTDM_ANALOG_START_GROUND:
                        cc.sigtype = ZT_SIG_FXOGS;
                        break;
                    default:
                        break;
                    }
                }
                break;
            case FTDM_CHAN_TYPE_FXO:
                {
                    switch(span->start_type) {
                    case FTDM_ANALOG_START_KEWL:
                        cc.sigtype = ZT_SIG_FXSKS;
                        break;
                    case FTDM_ANALOG_START_LOOP:
                        cc.sigtype = ZT_SIG_FXSLS;
                        break;
                    case FTDM_ANALOG_START_GROUND:
                        cc.sigtype = ZT_SIG_FXSGS;
                        break;
                    default:
                        break;
                    }
                }
                break;
            default:
                break;
            }

//            if (ioctl(CONTROL_FD, codes.CHANCONFIG, &cc)) {
//                ftdm_log(FTDM_LOG_WARNING, "this ioctl fails in older zaptel but is harmless if you used ztcfg\n[device chan %d fd %d (%s)]\n", x, CONTROL_FD, strerror(errno));
//            }
        }

//        if (ioctl(sockfd, codes.GET_PARAMS, &samap) < 0) {
//            ftdm_log(FTDM_LOG_ERROR, "failure configuring device as FreeTDM device %d:%d fd:%d\n", ftdmchan->span_id, ftdmchan->chan_id, sockfd);
//            close(sockfd);
//            continue;
//        }

        ftdm_log(FTDM_LOG_INFO, "configuring device channel %d as FreeTDM device %d:%d fd:%d\n", x, ftdmchan->span_id, ftdmchan->chan_id, ftdmchan->sockfd);

        ftdmchan->rate = 8000;
        ftdmchan->physical_span_id = samap.span_no;
        ftdmchan->physical_chan_id = samap.chan_no;

        if (type == FTDM_CHAN_TYPE_FXS || type == FTDM_CHAN_TYPE_FXO || type == FTDM_CHAN_TYPE_EM || type == FTDM_CHAN_TYPE_B) {
            if (samap.g711_type == ZT_G711_ALAW) {
                ftdmchan->native_codec = ftdmchan->effective_codec = FTDM_CODEC_ALAW;
            } else if (samap.g711_type == ZT_G711_MULAW) {
                ftdmchan->native_codec = ftdmchan->effective_codec = FTDM_CODEC_ULAW;
            } else {
                int type;

                if (ftdmchan->span->trunk_type == FTDM_TRUNK_E1) {
                    type = FTDM_CODEC_ALAW;
                } else {
                    type = FTDM_CODEC_ULAW;
                }

                ftdmchan->native_codec = ftdmchan->effective_codec = type;

            }
        }

        samap.wink_time = sama_globals.wink_ms;
        samap.flash_time = sama_globals.flash_ms;

//        if (ioctl(sockfd, codes.SET_PARAMS, &samap) < 0) {
//            ftdm_log(FTDM_LOG_ERROR, "failure configuring device as FreeTDM device %d:%d fd:%d\n", ftdmchan->span_id, ftdmchan->chan_id, sockfd);
//            close(sockfd);
//            continue;
//        }

//        mode = ZT_TONEDETECT_ON | ZT_TONEDETECT_MUTE;
//        if (ioctl(sockfd, codes.TONEDETECT, &mode)) {
//            ftdm_log(FTDM_LOG_DEBUG, "HW DTMF not available on FreeTDM device %d:%d fd:%d\n", ftdmchan->span_id, ftdmchan->chan_id, sockfd);
//        } else {
//            ftdm_log(FTDM_LOG_DEBUG, "HW DTMF available on FreeTDM device %d:%d fd:%d\n", ftdmchan->span_id, ftdmchan->chan_id, sockfd);
//            ftdm_channel_set_feature(ftdmchan, FTDM_CHANNEL_FEATURE_DTMF_DETECT);
//            mode = 0;
//            ioctl(sockfd, codes.TONEDETECT, &mode);
//        }

        if (!ftdm_strlen_zero(name)) {
            ftdm_copy_string(ftdmchan->chan_name, name, sizeof(ftdmchan->chan_name));
        }
        if (!ftdm_strlen_zero(number)) {
            ftdm_copy_string(ftdmchan->chan_number, number, sizeof(ftdmchan->chan_number));
        }
    }
	
    return 1;
}

static void create_host(uint32_t port, struct sockaddr_in *address) {
    address->sin_family = AF_INET;
    // TODO: Host could be configured here.
    address->sin_addr.s_addr = htonl(INADDR_ANY);
    address->sin_port = htons(port);

    ftdm_log(FTDM_LOG_INFO, "Sama host configured is %d, %d, %d\n", address->sin_addr.s_addr, address->sin_family, address->sin_port);
}

/**
 * \brief Initialises a freetdm Zaptel/DAHDI span from a configuration string
 * \param span FreeTDM span
 * \param str Configuration string
 * \param type FreeTDM span type
 * \param name FreeTDM span name
 * \param number FreeTDM span number
 * \return Success or failure
 */
static FIO_CONFIGURE_SPAN_FUNCTION(sama_configure_span)
{

	int items, i;
	char *mydata, *item_list[10];
	char *ch, *mx;
	unsigned char cas_bits = 0;
	int channo;
	int top = 0;
	unsigned configured = 0;

    ftdm_log(FTDM_LOG_INFO, "Span configuration start.\n");
	assert(str != NULL);

	mydata = ftdm_strdup(str);
	assert(mydata != NULL);

    if (CONTROL_FD == FTDM_INVALID_SOCKET && DATA_FD == FTDM_INVALID_SOCKET) {
        sama_run_sockets(sama_globals.signaling_host, &CONTROL_FD);
        sama_run_sockets(sama_globals.data_host, &DATA_FD);
        if (CONTROL_FD == FTDM_INVALID_SOCKET || DATA_FD == FTDM_INVALID_SOCKET) {
            ftdm_log(FTDM_LOG_CRIT, "Problem in run server.\n");
            return configured;
        }
        ftdm_log(FTDM_LOG_INFO, "s port is %d, d port is %d\n", CONTROL_FD, DATA_FD);
    }

	items = ftdm_separate_string(mydata, ',', item_list, (sizeof(item_list) / sizeof(item_list[0])));

	for(i = 0; i < items; i++) {
		ch = item_list[i];

		if (!(ch)) {
			ftdm_log(FTDM_LOG_ERROR, "Invalid input\n");
			continue;
		}

		channo = atoi(ch);
		
		if (channo < 0) {
			ftdm_log(FTDM_LOG_ERROR, "Invalid channel number %d\n", channo);
			continue;
		}

		if ((mx = strchr(ch, '-'))) {
			mx++;
			top = atoi(mx) + 1;
		} else {
			top = channo + 1;
		}
		
		
		if (top < 0) {
			ftdm_log(FTDM_LOG_ERROR, "Invalid range number %d\n", top);
			continue;
		}
		if (FTDM_CHAN_TYPE_CAS == type && ftdm_config_get_cas_bits(ch, &cas_bits)) {
			ftdm_log(FTDM_LOG_ERROR, "Failed to get CAS bits in CAS channel\n");
			continue;
		}
        configured += sama_open_range(span, channo, top, type, name, number, cas_bits);

	}
	
	ftdm_safe_free(mydata);

	return configured;

}

/**
 * \brief Process configuration variable for a Zaptel/DAHDI profile
 * \param category Wanpipe profile name
 * \param var Variable name
 * \param val Variable value
 * \param lineno Line number from configuration file
 * \return Success
 */
static FIO_CONFIGURE_FUNCTION(sama_configure)
{

    int num;

    ftdm_log(FTDM_LOG_ERROR, "Sama configuration start.\n");

    if (!strcasecmp(category, "defaults")) {
        if (!strcasecmp(var, "data_port_no")) {
            num = atoi(val);
            create_host(num, &(sama_globals.data_host));
            ftdm_log(FTDM_LOG_INFO, "Sama data port configured is %d\n", num);
            ftdm_log(FTDM_LOG_INFO, "Sama data host configured is %d, %d, %d\n", sama_globals.data_host.sin_addr.s_addr, sama_globals.data_host.sin_family, sama_globals.data_host.sin_port);
        } else if (!strcasecmp(var, "signaling_port_no")) {
            num = atoi(val);
            create_host(num, &(sama_globals.signaling_host));
            ftdm_log(FTDM_LOG_INFO, "Sama signaling port configured is %d\n", num);
            ftdm_log(FTDM_LOG_INFO, "Sama signaling host configured is %d, %d, %d\n", sama_globals.signaling_host.sin_addr.s_addr, sama_globals.signaling_host.sin_family, sama_globals.signaling_host.sin_port);
        } else if (!strcasecmp(var, "ip_address")) {
            sama_globals.data_host.sin_addr.s_addr = inet_addr(val);
            sama_globals.signaling_host.sin_addr.s_addr = inet_addr(val);
            ftdm_log(FTDM_LOG_INFO, "Sama signaling host configured is %d, %d, %d\n", sama_globals.signaling_host.sin_addr.s_addr, sama_globals.signaling_host.sin_family, sama_globals.signaling_host.sin_port);
            ftdm_log(FTDM_LOG_INFO, "Sama data host configured is %d, %d, %d\n", sama_globals.data_host.sin_addr.s_addr, sama_globals.data_host.sin_family, sama_globals.data_host.sin_port);
        } else {
                ftdm_log(FTDM_LOG_WARNING, "Ignoring unknown setting '%s'\n", var);
        }
    }

    return FTDM_SUCCESS;
}

/**
 * \brief Opens a Zaptel/DAHDI channel
 * \param ftdmchan Channel to open
 * \return Success or failure
 */
static FIO_OPEN_FUNCTION(sama_open)
{
	ftdm_channel_set_feature(ftdmchan, FTDM_CHANNEL_FEATURE_INTERVAL);
    ftdm_log(FTDM_LOG_WARNING, "Sama open\n");

	if (ftdmchan->type == FTDM_CHAN_TYPE_DQ921 || ftdmchan->type == FTDM_CHAN_TYPE_DQ931) {
		ftdmchan->native_codec = ftdmchan->effective_codec = FTDM_CODEC_NONE;
	} else {
        int blocksize = sama_globals.codec_ms * (ftdmchan->rate / 1000);
        int err;
        if ((err = sama_ioctl(CONTROL_FD, codes.SET_BLOCKSIZE, &blocksize))) {
			snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "%s", strerror(errno));
            ftdm_log(FTDM_LOG_CRIT, "Fail in ioctl. %s\n", ftdmchan->last_error);
			return FTDM_FAIL;
		} else {
			ftdmchan->effective_interval = ftdmchan->native_interval;
			ftdmchan->packet_len = blocksize;
			ftdmchan->native_codec = ftdmchan->effective_codec;
		}
		
		if (ftdmchan->type == FTDM_CHAN_TYPE_B) {
			int one = 1;
            if (sama_ioctl(CONTROL_FD, codes.AUDIOMODE, &one)) {
				snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "%s", strerror(errno));
				ftdm_log(FTDM_LOG_ERROR, "%s\n", ftdmchan->last_error);
				return FTDM_FAIL;
			}
		}
        if (sama_globals.rxgain || sama_globals.txgain) {
            struct sama_gains gains;
            memset(&gains, 0, sizeof(gains));

            gains.chan_no = ftdmchan->physical_chan_id;
            sama_build_gains(&gains, sama_globals.rxgain, sama_globals.txgain, ftdmchan->native_codec);

            if (sama_globals.rxgain)
                ftdm_log(FTDM_LOG_INFO, "Setting rxgain to %f on channel %d\n", sama_globals.rxgain, gains.chan_no);

            if (sama_globals.txgain)
                ftdm_log(FTDM_LOG_INFO, "Setting txgain to %f on channel %d\n", sama_globals.txgain, gains.chan_no);

//            if (ioctl(ftdmchan->sockfd, codes.SETGAINS, &gains) < 0) {
//                ftdm_log(FTDM_LOG_ERROR, "failure configuring device as FreeTDM device %d:%d fd:%d\n", ftdmchan->span_id, ftdmchan->chan_id, ftdmchan->sockfd);
//            }
        }

		if (1) {
            int len = sama_globals.eclevel;
			if (len) {
                ftdm_log(FTDM_LOG_INFO, "Setting echo cancel to %d taps for %d:%d\n", len, ftdmchan->span_id, ftdmchan->chan_id);
			} else {
				ftdm_log(FTDM_LOG_INFO, "Disable echo cancel for %d:%d\n", ftdmchan->span_id, ftdmchan->chan_id);
			}
            if (sama_ioctl(CONTROL_FD, codes.ECHOCANCEL, &len)) {
				ftdm_log(FTDM_LOG_WARNING, "Echo cancel not available for %d:%d\n", ftdmchan->span_id, ftdmchan->chan_id);
            } else if (sama_globals.etlevel > 0) {
                len = sama_globals.etlevel;
                if (sama_ioctl(CONTROL_FD, codes.ECHOTRAIN, &len)) {
					ftdm_log(FTDM_LOG_WARNING, "Echo training not available for %d:%d\n", ftdmchan->span_id, ftdmchan->chan_id);
				}
			}
		}
	}
	return FTDM_SUCCESS;
}

/**
 * \brief Closes Zaptel/DAHDI channel
 * \param ftdmchan Channel to close
 * \return Success
 */
static FIO_CLOSE_FUNCTION(sama_close)
{
    ftdm_log(FTDM_LOG_WARNING, "Sama close\n");
	if (ftdmchan->type == FTDM_CHAN_TYPE_B) {
		int value = 0;	/* disable audio mode */
        if (sama_ioctl(CONTROL_FD, codes.AUDIOMODE, &value)) {
			snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "%s", strerror(errno));
			ftdm_log(FTDM_LOG_ERROR, "%s\n", ftdmchan->last_error);
			return FTDM_FAIL;
		}
	}
	return FTDM_SUCCESS;
}

/**
 * \brief Executes a FreeTDM command on a Zaptel/DAHDI channel
 * \param ftdmchan Channel to execute command on
 * \param command FreeTDM command to execute
 * \param obj Object (unused)
 * \return Success or failure
 */
static FIO_COMMAND_FUNCTION(sama_command)
{
    sama_params_t samap;
	int err = 0;

    ftdm_log(FTDM_LOG_WARNING, "Sama command\n");
    ftdm_log(FTDM_LOG_INFO, "Sama command %d\n", command);
    memset(&samap, 0, sizeof(samap));

	switch(command) {
	case FTDM_COMMAND_ENABLE_ECHOCANCEL:
		{
			int level = FTDM_COMMAND_OBJ_INT;
            err = sama_ioctl(CONTROL_FD, codes.ECHOCANCEL, &level);
			FTDM_COMMAND_OBJ_INT = level;
		}
	case FTDM_COMMAND_DISABLE_ECHOCANCEL:
		{
			int level = 0;
            err = sama_ioctl(CONTROL_FD, codes.ECHOCANCEL, &level);
			FTDM_COMMAND_OBJ_INT = level;
		}
		break;
	case FTDM_COMMAND_ENABLE_ECHOTRAIN:
		{
			int level = FTDM_COMMAND_OBJ_INT;
            err = sama_ioctl(CONTROL_FD, codes.ECHOTRAIN, &level);
			FTDM_COMMAND_OBJ_INT = level;
		}
	case FTDM_COMMAND_DISABLE_ECHOTRAIN:
		{
			int level = 0;
            err = sama_ioctl(CONTROL_FD, codes.ECHOTRAIN, &level);
			FTDM_COMMAND_OBJ_INT = level;
		}
		break;
	case FTDM_COMMAND_OFFHOOK:
		{
			int command = ZT_OFFHOOK;
            if (sama_ioctl(CONTROL_FD, codes.HOOK, &command)) {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_ERROR, "OFFHOOK Failed");
				return FTDM_FAIL;
			}
			ftdm_log_chan_msg(ftdmchan, FTDM_LOG_DEBUG, "Channel is now offhook\n");
			ftdm_set_flag_locked(ftdmchan, FTDM_CHANNEL_OFFHOOK);
		}
		break;
	case FTDM_COMMAND_ONHOOK:
		{
			int command = ZT_ONHOOK;
            if (sama_ioctl(CONTROL_FD, codes.HOOK, &command)) {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_ERROR, "ONHOOK Failed");
				return FTDM_FAIL;
			}
			ftdm_log_chan_msg(ftdmchan, FTDM_LOG_DEBUG, "Channel is now onhook\n");
			ftdm_clear_flag_locked(ftdmchan, FTDM_CHANNEL_OFFHOOK);
		}
		break;
	case FTDM_COMMAND_FLASH:
		{
			int command = ZT_FLASH;
            if (sama_ioctl(CONTROL_FD, codes.HOOK, &command)) {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_ERROR, "FLASH Failed");
				return FTDM_FAIL;
			}
		}
		break;
	case FTDM_COMMAND_WINK:
		{
			int command = ZT_WINK;
            if (sama_ioctl(CONTROL_FD, codes.HOOK, &command)) {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_ERROR, "WINK Failed");
				return FTDM_FAIL;
			}
		}
		break;
	case FTDM_COMMAND_GENERATE_RING_ON:
		{
			int command = ZT_RING;
            if (sama_ioctl(CONTROL_FD, codes.HOOK, &command)) {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_ERROR, "RING Failed");
				return FTDM_FAIL;
			}
			ftdm_set_flag_locked(ftdmchan, FTDM_CHANNEL_RINGING);
		}
		break;
	case FTDM_COMMAND_GENERATE_RING_OFF:
		{
			int command = ZT_RINGOFF;
            if (sama_ioctl(CONTROL_FD, codes.HOOK, &command)) {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_ERROR, "Ring-off Failed");
				return FTDM_FAIL;
			}
			ftdm_clear_flag_locked(ftdmchan, FTDM_CHANNEL_RINGING);
		}
		break;
	case FTDM_COMMAND_GET_INTERVAL:
		{
            int data = (int)ftdmchan->packet_len;
            if (!(err = sama_ioctl(CONTROL_FD, codes.GET_BLOCKSIZE, &data))) {
                ftdmchan->packet_len = data;
				ftdmchan->native_interval = ftdmchan->packet_len / 8;
				if (ftdmchan->effective_codec == FTDM_CODEC_SLIN) {
					ftdmchan->packet_len *= 2;
				}
				FTDM_COMMAND_OBJ_INT = ftdmchan->native_interval;
            }

		}
		break;
	case FTDM_COMMAND_SET_INTERVAL: 
		{
			int interval = FTDM_COMMAND_OBJ_INT;
			int len = interval * 8;

            if (!(err = sama_ioctl(CONTROL_FD, codes.SET_BLOCKSIZE, &len))) {
				ftdmchan->packet_len = len;
				ftdmchan->effective_interval = ftdmchan->native_interval = ftdmchan->packet_len / 8;

				if (ftdmchan->effective_codec == FTDM_CODEC_SLIN) {
					ftdmchan->packet_len *= 2;
				}
			}
		}
		break;
	case FTDM_COMMAND_SET_CAS_BITS:
		{
			int bits = FTDM_COMMAND_OBJ_INT;
            err = sama_ioctl(CONTROL_FD, codes.SETTXBITS, &bits);
		}
		break;
    /*case FTDM_COMMAND_GET_CAS_BITS:
		{
			err = ioctl(ftdmchan->sockfd, codes.GETRXBITS, &ftdmchan->rx_cas_bits);
			if (!err) {
				FTDM_COMMAND_OBJ_INT = ftdmchan->rx_cas_bits;
			}
		}
        break;*/
	case FTDM_COMMAND_FLUSH_TX_BUFFERS:
		{
			int flushmode = ZT_FLUSH_WRITE;
            err = sama_ioctl(CONTROL_FD, codes.FLUSH, &flushmode);
		}
		break;
    /*case FTDM_COMMAND_SET_POLARITY:
        {
            ftdm_polarity_t polarity = FTDM_COMMAND_OBJ_INT;
            err = ioctl(ftdmchan->sockfd, codes.SETPOLARITY, polarity);
            if (!err) {
                ftdmchan->polarity = polarity;
            }
        }
        break;*/
	case FTDM_COMMAND_FLUSH_RX_BUFFERS:
		{
			int flushmode = ZT_FLUSH_READ;
            err = sama_ioctl(CONTROL_FD, codes.FLUSH, &flushmode);
		}
		break;
	case FTDM_COMMAND_FLUSH_BUFFERS:
		{
			int flushmode = ZT_FLUSH_BOTH;
            err = sama_ioctl(CONTROL_FD, codes.FLUSH, &flushmode);
		}
		break;
	case FTDM_COMMAND_SET_RX_QUEUE_SIZE:
	case FTDM_COMMAND_SET_TX_QUEUE_SIZE:
		/* little white lie ... eventually we can implement this, in the meantime, not worth the effort
		   and this is only used by some sig modules such as ftmod_r2 to behave bettter under load */
		err = 0;
		break;
	case FTDM_COMMAND_ENABLE_DTMF_DETECT:
		{
            sama_tone_mode_t mode = ZT_TONEDETECT_ON | ZT_TONEDETECT_MUTE;
            int data = (int)mode;
            err = sama_ioctl(CONTROL_FD, codes.TONEDETECT, &data);
		}
		break;
	case FTDM_COMMAND_DISABLE_DTMF_DETECT:
		{
            sama_tone_mode_t mode = 0;
            int data = (int)mode;
            err = sama_ioctl(CONTROL_FD, codes.TONEDETECT, &data);
		}
		break;
	default:
		err = FTDM_NOTIMPL;
		break;
	};

	if (err && err != FTDM_NOTIMPL) {
		snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "%s", strerror(errno));
		return FTDM_FAIL;
	}


	return err == 0 ? FTDM_SUCCESS : err;
}

/**
 * \brief Gets alarms from a Zaptel/DAHDI channel
 * \param ftdmchan Channel to get alarms from
 * \return Success or failure
 */
static FIO_GET_ALARMS_FUNCTION(sama_get_alarms)
{
    struct sama_spaninfo info;
    sama_params_t params;

    ftdm_log(FTDM_LOG_WARNING, "Sama alarms\n");
	memset(&info, 0, sizeof(info));
	info.span_no = ftdmchan->physical_span_id;

	memset(&params, 0, sizeof(params));

    /*if (ioctl(CONTROL_FD, codes.SPANSTAT, &info)) {
		snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "ioctl failed (%s)", strerror(errno));
		snprintf(ftdmchan->span->last_error, sizeof(ftdmchan->span->last_error), "ioctl failed (%s)", strerror(errno));
		return FTDM_FAIL;
    }*/

    ftdmchan->alarm_flags = info.alarms;

//    /* get channel alarms if span has no alarms */
//    if (info.alarms == FTDM_ALARM_NONE) {
//        if (ioctl(ftdmchan->sockfd, codes.GET_PARAMS, &params)) {
//            snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "ioctl failed (%s)", strerror(errno));
//            snprintf(ftdmchan->span->last_error, sizeof(ftdmchan->span->last_error), "ioctl failed (%s)", strerror(errno));
//            return FTDM_FAIL;
//        }

//        if (params.chan_alarms > 0) {
//            if (params.chan_alarms == DAHDI_ALARM_YELLOW) {
//                ftdmchan->alarm_flags = FTDM_ALARM_YELLOW;
//            }
//            else if (params.chan_alarms == DAHDI_ALARM_BLUE) {
//                ftdmchan->alarm_flags = FTDM_ALARM_BLUE;
//            }
//            else {
//                ftdmchan->alarm_flags = FTDM_ALARM_RED;
//            }
//        }
//    }

	return FTDM_SUCCESS;
}

#define ftdm_sama_set_event_pending(fchan) \
	do { \
		ftdm_set_io_flag(fchan, FTDM_CHANNEL_IO_EVENT); \
		fchan->last_event_time = ftdm_current_time_in_ms(); \
	} while (0);

#define ftdm_sama_store_chan_event(fchan, revent) \
	do { \
		if (fchan->io_data) { \
			ftdm_log_chan(fchan, FTDM_LOG_WARNING, "Dropping event %d, not retrieved on time\n", revent); \
		} \
        fchan->io_data = (void *)sama_event_id; \
        ftdm_sama_set_event_pending(fchan); \
	} while (0);

/**
 * \brief Waits for an event on a Zaptel/DAHDI channel
 * \param ftdmchan Channel to open
 * \param flags Type of event to wait for
 * \param to Time to wait (in ms)
 * \return Success, failure or timeout
 */
static FIO_WAIT_FUNCTION(sama_wait)
{
    int32_t inflags = 0;
    int32_t event = FTDM_NO_FLAGS;

    ftdm_log(FTDM_LOG_WARNING, "Sama wait for flag %d to %d\n", (int)*flags, to);

    if (*flags & FTDM_READ) {
        inflags |= POLLIN;
    }

    if (*flags & FTDM_WRITE) {
        inflags |= POLLOUT;
    }

    if (*flags & FTDM_EVENTS) {
        inflags |= POLLPRI;
    }

    if (sama_has_event(&sama_span_params[0], inflags) == 0) {
        event = pop_sama_event(&sama_span_params[0], inflags);

        *flags = FTDM_NO_FLAGS;
        inflags = event;

        ftdm_log(FTDM_LOG_WARNING, "event is %d\n", (int)event);

        if (inflags & POLLERR) {
            snprintf(ftdmchan->last_error, sizeof(ftdmchan->last_error), "Poll failed");
            ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "Failed to poll Sama device: %s\n", strerror(errno));
            return FTDM_FAIL;
        }

        if (inflags & POLLIN) {
            *flags |= FTDM_READ;
        }

        if (inflags & POLLOUT) {
            *flags |= FTDM_WRITE;
        }

        if ((inflags & POLLPRI) || (ftdmchan->io_data && (*flags & FTDM_EVENTS))) {
            *flags |= FTDM_EVENTS;
        }

        return FTDM_SUCCESS;
    }

    usleep(to * 1000);
    return FTDM_TIMEOUT;
}

/**
 * \brief Checks for events on a Zaptel/DAHDI span
 * \param span Span to check for events
 * \param ms Time to wait for event
 * \return Success if event is waiting or failure if not
 */
FIO_SPAN_POLL_EVENT_FUNCTION(sama_poll_event)
{
    uint32_t i, k = 0, event = FTDM_NO_FLAGS;

    ftdm_log(FTDM_LOG_WARNING, "Sama poll event\n");
	ftdm_unused_arg(poll_events);

    usleep(ms * 1000);

    if (sama_has_event(&sama_span_params[0], 2) == 0) {
        ftdm_log(FTDM_LOG_WARNING, "Sama has event.\n");

        for(i = 1; i <= span->chan_count; i++) {

            ftdm_channel_lock(span->channels[i]);

            event = pop_sama_event(&sama_span_params[0], 2);
            if ((event & POLLPRI) || (span->channels[i]->io_data)) {
                ftdm_sama_set_event_pending(span->channels[i]);
                k++;
            }
            if (event & POLLIN) {
                ftdm_set_io_flag(span->channels[i], FTDM_CHANNEL_IO_READ);
            }
            if (event & POLLOUT) {
                ftdm_set_io_flag(span->channels[i], FTDM_CHANNEL_IO_WRITE);
            }

            ftdm_channel_unlock(span->channels[i]);

        }

        if (!k) {
            snprintf(span->last_error, sizeof(span->last_error), "no matching descriptor");
        }

        return k ? FTDM_SUCCESS : FTDM_FAIL;
    }

    return FTDM_TIMEOUT;
}

static __inline__ int handle_dtmf_event(ftdm_channel_t *fchan, sama_event_t sama_event_id)
{
    ftdm_log(FTDM_LOG_WARNING, "Sama dtmf event\n");
    if ((sama_event_id & SAMA_EVENT_DTMFUP)) {
        int digit = (sama_event_id & (~SAMA_EVENT_DTMFUP));
		char tmp_dtmf[2] = { digit, 0 };
		ftdm_log_chan(fchan, FTDM_LOG_DEBUG, "DTMF UP [%d]\n", digit);
		ftdm_channel_queue_dtmf(fchan, tmp_dtmf);
		return 0;
    } else if ((sama_event_id & SAMA_EVENT_DTMFDOWN)) {
        int digit = (sama_event_id & (~SAMA_EVENT_DTMFDOWN));
		ftdm_log_chan(fchan, FTDM_LOG_DEBUG, "DTMF DOWN [%d]\n", digit);
		return 0;
	} else {
		return -1;
	}
}

/**
 * \brief Process an event from a ftdmchan and set the proper OOB event_id. The channel must be locked.
 * \param fchan Channel to retrieve event from
 * \param event_id Pointer to OOB event id
 * \param sama_event_id Zaptel event id
 * \return FTDM_SUCCESS or FTDM_FAIL
 */
static __inline__ ftdm_status_t sama_channel_process_event(ftdm_channel_t *fchan, ftdm_oob_event_t *event_id, sama_event_t sama_event_id)
{
    ftdm_log_chan(fchan, FTDM_LOG_DEBUG, "Processing sama hardware event %d\n", sama_event_id);
    switch(sama_event_id) {
    case SAMA_EVENT_RINGEROFF:
		{
            ftdm_log_chan_msg(fchan, FTDM_LOG_DEBUG, "SAMA RINGER OFF\n");
			*event_id = FTDM_OOB_NOOP;
		}
		break;
    case SAMA_EVENT_RINGERON:
		{
            ftdm_log_chan_msg(fchan, FTDM_LOG_DEBUG, "SAMA RINGER ON\n");
			*event_id = FTDM_OOB_NOOP;
		}
		break;
    case SAMA_EVENT_RINGBEGIN:
		{
			*event_id = FTDM_OOB_RING_START;
		}
		break;
    case SAMA_EVENT_ONHOOK:
		{
			*event_id = FTDM_OOB_ONHOOK;
		}
		break;
    case SAMA_EVENT_WINKFLASH:
		{
			if (fchan->state == FTDM_CHANNEL_STATE_DOWN || fchan->state == FTDM_CHANNEL_STATE_DIALING) {
				*event_id = FTDM_OOB_WINK;
			} else {
				*event_id = FTDM_OOB_FLASH;
			}
		}
		break;
    case SAMA_EVENT_RINGOFFHOOK:
		{
			*event_id = FTDM_OOB_NOOP;
			if (fchan->type == FTDM_CHAN_TYPE_FXS || (fchan->type == FTDM_CHAN_TYPE_EM && fchan->state != FTDM_CHANNEL_STATE_UP)) {
				if (fchan->type != FTDM_CHAN_TYPE_EM) {
					/* In E&M we're supposed to set this flag only when the local side goes offhook, not the remote */
					ftdm_set_flag_locked(fchan, FTDM_CHANNEL_OFFHOOK);
				}

				/* For E&M let's count the ring count (it seems sometimes we receive RINGOFFHOOK once before the other end
				 * answers, then another RINGOFFHOOK when the other end answers?? anyways, now we count rings before delivering the
				 * offhook event ... the E&M signaling code in ftmod_analog_em also polls the RBS bits looking for answer, just to
				 * be safe and not rely on this event, so even if this event does not arrive, when there is answer supervision
				 * the analog signaling code should detect the cas persistance pattern and answer */
				if (fchan->type == FTDM_CHAN_TYPE_EM && ftdm_test_flag(fchan, FTDM_CHANNEL_OUTBOUND)) {
					fchan->ring_count++;
					/* perhaps some day we'll make this configurable, but since I am not even sure what the hell is going on
					 * no point in making a configuration option for something that may not be technically correct */
					if (fchan->ring_count == 2) {
						*event_id = FTDM_OOB_OFFHOOK;
					}
				} else {
					*event_id = FTDM_OOB_OFFHOOK;
				}
			} else if (fchan->type == FTDM_CHAN_TYPE_FXO) {
				*event_id = FTDM_OOB_RING_START;
			}
		}
		break;
    case SAMA_EVENT_ALARM:
		{
			*event_id = FTDM_OOB_ALARM_TRAP;
		}
		break;
    case SAMA_EVENT_NOALARM:
		{
			*event_id = FTDM_OOB_ALARM_CLEAR;
		}
		break;
    case SAMA_EVENT_BITSCHANGED:
		{
			*event_id = FTDM_OOB_CAS_BITS_CHANGE;
			int bits = 0;
            int err = sama_ioctl(CONTROL_FD, codes.GETRXBITS, &bits);
			if (err) {
				return FTDM_FAIL;
			}
			fchan->rx_cas_bits = bits;
		}
		break;
    case SAMA_EVENT_BADFCS:
		{
            ftdm_log_chan_msg(fchan, FTDM_LOG_ERROR, "Bad frame checksum (SAMA_EVENT_BADFCS)\n");
			*event_id = FTDM_OOB_NOOP;	/* What else could we do? */
		}
		break;
    case SAMA_EVENT_OVERRUN:
		{
            ftdm_log_chan_msg(fchan, FTDM_LOG_ERROR, "HDLC frame overrun (SAMA_EVENT_OVERRUN)\n");
			*event_id = FTDM_OOB_NOOP;	/* What else could we do? */
		}
		break;
    case SAMA_EVENT_ABORT:
		{
            ftdm_log_chan_msg(fchan, FTDM_LOG_ERROR, "HDLC abort frame received (SAMA_EVENT_ABORT)\n");
			*event_id = FTDM_OOB_NOOP;	/* What else could we do? */
		}
		break;
    case SAMA_EVENT_POLARITY:
		{
            ftdm_log_chan_msg(fchan, FTDM_LOG_ERROR, "Got polarity reverse (SAMA_EVENT_POLARITY)\n");
			*event_id = FTDM_OOB_POLARITY_REVERSE;
		}
		break;
    case SAMA_EVENT_NONE:
		{
			ftdm_log_chan_msg(fchan, FTDM_LOG_DEBUG, "No event\n");
			*event_id = FTDM_OOB_NOOP;
		}
		break;
	default:
		{
            if (handle_dtmf_event(fchan, sama_event_id)) {
                ftdm_log_chan(fchan, FTDM_LOG_WARNING, "Unhandled event %d\n", sama_event_id);
				*event_id = FTDM_OOB_INVALID;
			} else {
				*event_id = FTDM_OOB_NOOP;
			}
		}
		break;
	}
	return FTDM_SUCCESS;
}

/**
 * \brief Retrieves an event from a ftdm channel
 * \param ftdmchan Channel to retrieve event from
 * \param event FreeTDM event to return
 * \return Success or failure
 */
FIO_CHANNEL_NEXT_EVENT_FUNCTION(sama_channel_next_event)
{
    ftdm_log(FTDM_LOG_WARNING, "Sama channel next event\n");
    uint32_t event_id = FTDM_OOB_INVALID;
    sama_event_t sama_event_id = 0;
    ftdm_span_t *span = ftdmchan->span;
    int data = 0;
    int status = -1;

    if (ftdm_test_io_flag(ftdmchan, FTDM_CHANNEL_IO_EVENT)) {
        ftdm_clear_io_flag(ftdmchan, FTDM_CHANNEL_IO_EVENT);
    }

    if (ftdmchan->io_data) {
        sama_event_id = (sama_event_t)ftdmchan->io_data;
        ftdmchan->io_data = NULL;
    } else if ((status = sama_ioctl(ftdmchan->sockfd, codes.GETEVENT, &data)) == -1) {
        ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "Failed retrieving event from channel: %s\n",
                strerror(errno));
        return FTDM_FAIL;
    }

    if (status != -1) {
        sama_event_id = (sama_event_t)data;
    }

    /* the core already locked the channel for us, so it's safe to call sama_channel_process_event() here */
    if ((sama_channel_process_event(ftdmchan, &event_id, sama_event_id)) != FTDM_SUCCESS) {
        ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "Failed to process DAHDI event %d from channel\n", sama_event_id);
        return FTDM_FAIL;
    }

    ftdmchan->last_event_time = 0;
    span->event_header.e_type = FTDM_EVENT_OOB;
    span->event_header.enum_id = event_id;
    span->event_header.channel = ftdmchan;
    *event = &span->event_header;
    return FTDM_SUCCESS;
    return FTDM_FAIL;
}

/**
 * \brief Retrieves an event from a Zaptel/DAHDI span
 * \param span Span to retrieve event from
 * \param event FreeTDM event to return
 * \return Success or failure
 */
FIO_SPAN_NEXT_EVENT_FUNCTION(sama_next_event)
{
	uint32_t i, event_id = FTDM_OOB_INVALID;
    sama_event_t sama_event_id = 0;
    int data = (int)sama_event_id;
    int status = 0;

    ftdm_log(FTDM_LOG_WARNING, "Sama next event\n");
	for (i = 1; i <= span->chan_count; i++) {
		ftdm_channel_t *fchan = span->channels[i];

		ftdm_channel_lock(fchan);

		if (!ftdm_test_io_flag(fchan, FTDM_CHANNEL_IO_EVENT)) {

			ftdm_channel_unlock(fchan);

			continue;
		}

		ftdm_clear_io_flag(fchan, FTDM_CHANNEL_IO_EVENT);

		if (fchan->io_data) {
            ftdm_log(FTDM_LOG_INFO, "Sama event from io.\n");
            sama_event_id = (sama_event_t)fchan->io_data;
			fchan->io_data = NULL;
        } else if ((status = sama_ioctl(CONTROL_FD, codes.GETEVENT, &data)) == -1) {
			ftdm_log_chan(fchan, FTDM_LOG_ERROR, "Failed to retrieve DAHDI event from channel: %s\n", strerror(errno));

			ftdm_channel_unlock(fchan);

			continue;
		}

        if (status != -1) {
            sama_event_id = (sama_event_t)data;
        }

        ftdm_log_chan(fchan, FTDM_LOG_DEBUG, "sama event id is %d.\n", sama_event_id);
        if ((sama_channel_process_event(fchan, &event_id, sama_event_id)) != FTDM_SUCCESS) {
            ftdm_log_chan(fchan, FTDM_LOG_ERROR, "Failed to process DAHDI event %d from channel\n", sama_event_id);

			ftdm_channel_unlock(fchan);

			return FTDM_FAIL;
		}

		fchan->last_event_time = 0;
		span->event_header.e_type = FTDM_EVENT_OOB;
		span->event_header.enum_id = event_id;
		span->event_header.channel = fchan;
		*event = &span->event_header;

		ftdm_channel_unlock(fchan);

		return FTDM_SUCCESS;
	}

	return FTDM_FAIL;
}

/**
 * \brief Reads data from a Zaptel/DAHDI channel
 * \param ftdmchan Channel to read from
 * \param data Data buffer
 * \param datalen Size of data buffer
 * \return Success, failure or timeout
 */
static FIO_READ_FUNCTION(sama_read)
{
	ftdm_ssize_t r = 0;
	int read_errno = 0;
	int errs = 0;
    FILE *f;

    ftdm_log(FTDM_LOG_WARNING, "Sama read. data len is: %d\n", (int)*datalen);
	while (errs++ < 30) {
		r = read(ftdmchan->sockfd, data, *datalen);
		if (r > 0) {
            f = fopen("/home/naser/Desktop/test/test-client/in_voice.freeswitch", "a");
            fwrite(data, r, 1, f);
            fclose(f);
			/* successful read, bail out now ... */
            ftdm_log(FTDM_LOG_WARNING, "Successful read data len is: %d.\n", (int)r);
			break;
		}

		/* Timeout ... retry after a bit */
		if (r == 0) {
            ftdm_log(FTDM_LOG_WARNING, "Timeout read.\n");
			ftdm_sleep(10);
			if (errs) errs--;
			continue;
		}

		/* This gotta be an error, save errno in case we do printf(), ioctl() or other operations which may reset it */
		read_errno = errno;
		if (read_errno == EAGAIN || read_errno == EINTR) {
			/* Reasonable to retry under those errors */
			continue;
		}

		/* When ELAST is returned, it means DAHDI has an out of band event ready and we won't be able to read anything until
		 * we retrieve the event using an ioctl(), so we try to retrieve it here ... */
		if (read_errno == ELAST) {
            sama_event_t sama_event_id = 0;
            int data = (int)sama_event_id;
            int status = -1;
            if ((status = sama_ioctl(CONTROL_FD, codes.GETEVENT, &data)) == -1) {
				ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "Failed retrieving event after ELAST on read: %s\n", strerror(errno));
				r = -1;
				break;
			}

            if (status != -1) {
                sama_event_id = (sama_event_t)data;
            }

            if (handle_dtmf_event(ftdmchan, sama_event_id)) {
				/* Enqueue this event for later */
                ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "Deferring event %d to be able to read data\n", sama_event_id);
                ftdm_sama_store_chan_event(ftdmchan, sama_event_id);
			} else {
				ftdm_log_chan_msg(ftdmchan, FTDM_LOG_DEBUG, "Skipping one IO read cycle due to DTMF event processing\n");
			}
			break;
		}

		/* Read error, keep going unless to many errors force us to abort ...*/
		ftdm_log(FTDM_LOG_ERROR, "IO read failed: %s\n", strerror(read_errno));
	}

    ftdm_log(FTDM_LOG_WARNING, "Sama read done.\n");

	if (r > 0) {
        *datalen = r;
		return FTDM_SUCCESS;
	}
	else if (read_errno == ELAST) {
		return FTDM_SUCCESS;
	}
	return r == 0 ? FTDM_TIMEOUT : FTDM_FAIL;
}

/**
 * \brief Writes data to a Zaptel/DAHDI channel
 * \param ftdmchan Channel to write to
 * \param data Data buffer
 * \param datalen Size of data buffer
 * \return Success or failure
 */
static FIO_WRITE_FUNCTION(sama_write)
{
	ftdm_ssize_t w = 0;
	ftdm_size_t bytes = *datalen;
    FILE *f;

    ftdm_log(FTDM_LOG_WARNING, "Sama write. data len is: %d\n", (int)*datalen);
	if (ftdmchan->type == FTDM_CHAN_TYPE_DQ921) {
		memset(data+bytes, 0, 2);
		bytes += 2;
	}

tryagain:
	w = write(ftdmchan->sockfd, data, bytes);
	
	if (w >= 0) {
		*datalen = w;
        f = fopen("/home/naser/Desktop/test/test-client/out_voice.freeswitch", "a");
        fwrite(data, w, 1, f);
        fclose(f);
        ftdm_log(FTDM_LOG_WARNING, "Successful write data len is: %d\n", (int)*datalen);
		return FTDM_SUCCESS;
	}

	if (errno == ELAST) {
        sama_event_t sama_event_id = 0;
        int data = (int)sama_event_id;
        int status = -1;
        // TODO: Check ftdmchan socket is correct?
        if ((status = sama_ioctl(CONTROL_FD, codes.GETEVENT, &data)) == -1) {
			ftdm_log_chan(ftdmchan, FTDM_LOG_ERROR, "Failed retrieving event after ELAST on write: %s\n", strerror(errno));
			return FTDM_FAIL;
		}

        if (status != -1) {
            sama_event_id = (sama_event_t)data;
        }

        if (handle_dtmf_event(ftdmchan, sama_event_id)) {
			/* Enqueue this event for later */
            ftdm_log_chan(ftdmchan, FTDM_LOG_DEBUG, "Deferring event %d to be able to write data\n", sama_event_id);
            ftdm_sama_store_chan_event(ftdmchan, sama_event_id);
		}

		goto tryagain;
	}

	return FTDM_FAIL;
}

/**
 * \brief Destroys a Zaptel/DAHDI Channel
 * \param ftdmchan Channel to destroy
 * \return Success
 */
static FIO_CHANNEL_DESTROY_FUNCTION(sama_channel_destroy)
{
    ftdm_log(FTDM_LOG_WARNING, "Sama channel destroy\n");
	close(ftdmchan->sockfd);
    ftdmchan->sockfd = SAMA_INVALID_SOCKET;
	return FTDM_SUCCESS;
}

/**
 * \brief Global FreeTDM IO interface for Zaptel/DAHDI
 */
static ftdm_io_interface_t sama_interface;

/**
 * \brief Loads Zaptel/DAHDI IO module
 * \param fio FreeTDM IO interface
 * \return Success or failure
 */
static FIO_IO_LOAD_FUNCTION(sama_init)
{
    assert(fio != NULL);
    memset(&sama_interface, 0, sizeof(sama_interface));
    memset(&sama_globals, 0, sizeof(sama_globals));

    ftdm_log(FTDM_LOG_ERROR, "Init function\n");

    memcpy(&codes, &sama_ioctl_codes, sizeof(codes));

    sama_globals.codec_ms = 20;
    sama_globals.wink_ms = 150;
    sama_globals.flash_ms = 750;
    sama_globals.eclevel = 0;
    sama_globals.etlevel = 0;
	
    sama_interface.name = "sama";
    sama_interface.configure = sama_configure;
    sama_interface.configure_span = sama_configure_span;
    sama_interface.open = sama_open;
    sama_interface.close = sama_close;
    sama_interface.command = sama_command;
    sama_interface.wait = sama_wait;
    sama_interface.read = sama_read;
    sama_interface.write = sama_write;
    sama_interface.poll_event = sama_poll_event;
    sama_interface.next_event = sama_next_event;
    sama_interface.channel_next_event = sama_channel_next_event;
    sama_interface.channel_destroy = sama_channel_destroy;
    sama_interface.get_alarms = sama_get_alarms;
    *fio = &sama_interface;

	return FTDM_SUCCESS;
}

/**
 * \brief Unloads Zaptel/DAHDI IO module
 * \return Success
 */
static FIO_IO_UNLOAD_FUNCTION(sama_destroy)
{
    ftdm_log(FTDM_LOG_WARNING, "Sama destroy\n");
	close(CONTROL_FD);
    memset(&sama_interface, 0, sizeof(sama_interface));
	return FTDM_SUCCESS;
}

/**
 * \brief FreeTDM Zaptel/DAHDI IO module definition
 */
ftdm_module_t ftdm_module = { 
    "sama",
    sama_init,
    sama_destroy,
};

/* For Emacs:
 * Local Variables:
 * mode:c
 * indent-tabs-mode:t
 * tab-width:4
 * c-basic-offset:4
 * End:
 * For VIM:
 * vim:set softtabstop=4 shiftwidth=4 tabstop=4 noet:
 */
