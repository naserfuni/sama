/*
 * Copyright (c) 2007-2014, Anthony Minessale II
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * * Neither the name of the original author; nor the names of any contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 * 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Contributors: 
 *
 * Moises Silva <moy@sangoma.com>
 * W McRoberts <fs@whmcr.com>
 *
 */

#ifndef FTDM_SAMA_H
#define FTDM_SAMA_H
#include "freetdm.h"
#include <sys/ioctl.h>
#include <poll.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#ifdef __sun
#include <unistd.h>
#include <sys/ioccom.h>
#include <stropts.h>
#endif

/* Hardware interface structures and defines */
/* Based on documentation of the structures required for the hardware interface */
/* from http://wiki.freeswitch.org/wiki/Zapata_ftdmtel_interface */

/* Structures */

/* Used with ioctl: ZT_GET_PARAMS and ZT_SET_PARAMS */
struct sama_params {
	int chan_no;			/* Channel Number							*/
	int span_no;			/* Span Number								*/
	int chan_position;		/* Channel Position							*/
	int sig_type;			/* Signal Type (read-only)					*/
	int sig_cap;			/* Signal Cap (read-only)					*/
	int receive_offhook;	/* Receive is offhook (read-only)			*/
	int receive_bits;		/* Number of bits in receive (read-only)	*/
	int transmit_bits;		/* Number of bits in transmit (read-only)	*/
	int transmit_hook_sig;	/* Transmit Hook Signal (read-only)			*/
	int receive_hook_sig;	/* Receive Hook Signal (read-only)			*/
	int g711_type;			/* Member of zt_g711_t (read-only)			*/
	int idlebits;			/* bits for the idle state (read-only)		*/
	char chan_name[40];		/* Channel Name								*/
	int prewink_time;
	int preflash_time;
	int wink_time;
	int flash_time;
	int start_time;
	int receive_wink_time;
	int receive_flash_time;
	int debounce_time;
	int pulse_break_time;
	int pulse_make_time;
	int pulse_after_time;
    /* latest version of this struct include chan_alarms field */
    uint32_t chan_alarms;
};

typedef struct sama_params sama_params_t;
typedef struct sama_socket_connection sama_socket_connection_t;
typedef struct sama_span_param sama_span_param_t;

struct sama_socket_connection {
    struct sockaddr_in address;
    ftdm_socket_t c_sockfd;
    ftdm_socket_t o_sockfd;
    int is_signal;
    int *chan_no;
    ftdm_channel_t *channel;
};

#define MAX_QUEUE_SIZE 100000

struct sama_span_param {
    sama_socket_connection_t signaling;
    sama_socket_connection_t data;
    int chan_no;
    int32_t pri_event_queue[MAX_QUEUE_SIZE];
    int32_t in_event_queue[MAX_QUEUE_SIZE];
    int32_t out_event_queue[MAX_QUEUE_SIZE];

    int32_t ioctl_data[MAX_QUEUE_SIZE];
    ftdm_mutex_t *pri_event_mutex;
    ftdm_mutex_t *in_event_mutex;
    ftdm_mutex_t *out_event_mutex;
    ftdm_mutex_t *ioctl_mutex;
};

static sama_span_param_t *sama_span_params;

/* Used with ioctl: ZT_CONFLINK, ZT_GETCONF and ZT_SETCONF */
struct sama_confinfo {
	int chan_no;			/* Channel Number, 0 for current */
	int conference_number;
	int conference_mode;
};

/* Used with ioctl: ZT_GETGAINS and ZT_SETGAINS */
struct sama_gains {
	int chan_no;						/* Channel Number, 0 for current	*/
	unsigned char receive_gain[256];	/* Receive gain table				*/
	unsigned char transmit_gain[256];	/* Transmit gain table				*/
};

/* Used with ioctl: ZT_SPANSTAT */
struct sama_spaninfo {
	int span_no;						/* span number (-1 to use name)				*/
	char name[20];						/* Name of span								*/
	char description[40];				/* Description of span						*/
	int alarms;							/* alarms status							*/
	int transmit_level;					/* Transmit level							*/
	int receive_level;					/* Receive level							*/
	int bpv_count;						/* Current BPV count						*/
	int crc4_count;						/* Current CRC4 error count					*/
	int ebit_count;						/* Current E-bit error count				*/
	int fas_count;						/* Current FAS error count					*/
	int irq_misses;						/* Current IRQ misses						*/
	int sync_src;						/* Span # of sync source (0 = free run)		*/
	int configured_chan_count;			/* Count of channels configured on the span	*/
	int channel_count;					/* Total count of channels on the span		*/
	int span_count;						/* Total count of ftdmtel spans on the system*/
    /* end v1 of the struct */
    /* as long as we don't use the fields below we should be ok regardless of the ftdmtel/dahdi version */
    int lbo;                            /* Line Build Out */
    int lineconfig;                     /* framing/coding */
    /* end of v2 of the struct */
    char lboname[40];                   /* Line Build Out in text form */
    char location[40];                  /* span's device location in system */
    char manufacturer[40];              /* manufacturer of span's device */
    char devicetype[40];                /* span's device type */
    int irq;                            /* span's device IRQ */
    int linecompat;                     /* signaling modes possible on this span */
    char spantype[6];                   /* type of span in text form */
};

struct sama_maintinfo {
	int span_no;						/* span number											*/
	int command;						/* Maintenance mode to set (from zt_maintenance_mode_t)	*/
};

struct sama_lineconfig {
/* Used in ZT_SPANCONFIG */
	int span;							/* Which span number (0 to use name)		*/
	char name[20];						/* Name of span to use						*/
	int lbo;							/* line build-outs							*/
	int lineconfig;						/* line config parameters (framing, coding) */
	int sync;							/* what level of sync source we are			*/
};

struct sama_chanconfig {
/* Used in ZT_CHANCONFIG */
	int chan;							/* Channel we're applying this to (0 to use name)										*/
	char name[40];						/* Name of channel to use																*/
	int sigtype;						/* Signal type																			*/
	int deflaw;							/* Default law (ZT_LAW_DEFAULT, ZT_LAW_MULAW, or ZT_LAW_ALAW							*/
	int master;							/* Master channel if sigtype is ZT_SLAVE												*/
	int idlebits;						/* Idle bits (if this is a CAS channel) or channel to monitor (if this is DACS channel) */
	char netdev_name[16];				/* name for the hdlc network device														*/
};

struct sama_bufferinfo {
/* used in ZT_SET_BUFINFO and ZT_GET_BUFINFO */
	int txbufpolicy;					/* Policy for handling receive buffers			*/
	int rxbufpolicy;					/* Policy for handling receive buffers			*/
	int numbufs;						/* How many buffers to use						*/
	int bufsize;						/* How big each buffer is						*/
	int readbufs;						/* How many read buffers are full (read-only)	*/
	int writebufs;						/* How many write buffers are full (read-only)	*/
};

/* Enumerations */

/* Values in zt_params structure for member g711_type */
typedef enum {
	ZT_G711_DEFAULT		= 0,	/* Default mulaw/alaw from the span */
	ZT_G711_MULAW		= 1,
	ZT_G711_ALAW		= 2
} sama_g711_t;

typedef enum {
    SAMA_EVENT_NONE			= 0,
    SAMA_EVENT_ONHOOK			= 1,
    SAMA_EVENT_RINGOFFHOOK	= 2,
    SAMA_EVENT_WINKFLASH		= 3,
    SAMA_EVENT_ALARM			= 4,
    SAMA_EVENT_NOALARM		= 5,
    SAMA_EVENT_ABORT			= 6,
    SAMA_EVENT_OVERRUN		= 7,
    SAMA_EVENT_BADFCS			= 8,
    SAMA_EVENT_DIALCOMPLETE	= 9,
    SAMA_EVENT_RINGERON		= 10,
    SAMA_EVENT_RINGEROFF		= 11,
    SAMA_EVENT_HOOKCOMPLETE	= 12,
    SAMA_EVENT_BITSCHANGED	= 13,
    SAMA_EVENT_PULSE_START	= 14,
    SAMA_EVENT_TIMER_EXPIRED	= 15,
    SAMA_EVENT_TIMER_PING		= 16,
    SAMA_EVENT_POLARITY		= 17,
    SAMA_EVENT_RINGBEGIN		= 18,
    SAMA_EVENT_DTMFDOWN		= (1 << 17),
    SAMA_EVENT_DTMFUP			= (1 << 18),
} sama_event_t;

typedef enum {
	ZT_FLUSH_READ			= 1,
	ZT_FLUSH_WRITE			= 2,
	ZT_FLUSH_BOTH			= (ZT_FLUSH_READ | ZT_FLUSH_WRITE),
	ZT_FLUSH_EVENT			= 4,
	ZT_FLUSH_ALL			= (ZT_FLUSH_READ | ZT_FLUSH_WRITE | ZT_FLUSH_EVENT)
} sama_flush_t;

typedef enum {
	ZT_IOMUX_READ			= 1,
	ZT_IOMUX_WRITE			= 2,
	ZT_IOMUX_WRITEEMPTY		= 4,
	ZT_IOMUX_SIGEVENT		= 8,
	ZT_IOMUX_NOWAIT			= 256
} sama_iomux_t;

typedef enum {
	ZT_ONHOOK				= 0,
	ZT_OFFHOOK				= 1,
	ZT_WINK					= 2,
	ZT_FLASH				= 3,
	ZT_START				= 4,
	ZT_RING					= 5,
	ZT_RINGOFF				= 6
} sama_hookstate_t;

typedef enum {
	ZT_MAINT_NONE			= 0, /* Normal Mode				*/
	ZT_MAINT_LOCALLOOP		= 1, /* Local Loopback			*/
	ZT_MAINT_REMOTELOOP		= 2, /* Remote Loopback			*/
	ZT_MAINT_LOOPUP			= 3, /* Send Loopup Code		*/
	ZT_MAINT_LOOPDOWN		= 4, /* Send Loopdown Code		*/
	ZT_MAINT_LOOPSTOP		= 5  /* Stop Sending Loop Codes	*/
} sama_maintenance_mode_t;

typedef enum {
/* Signalling type */
ZT_SIG_NONE					= 0,						/* chan not configured. */

ZT_SIG_FXSLS				= ((1 << 0) | (1 << 13)),	/* FXS, Loopstart */
ZT_SIG_FXSGS				= ((1 << 1) | (1 << 13)),	/* FXS, Groundstart */
ZT_SIG_FXSKS				= ((1 << 2) | (1 << 13)),	/* FXS, Kewlstart */
ZT_SIG_FXOLS				= ((1 << 3) | (1 << 12)),	/* FXO, Loopstart */
ZT_SIG_FXOGS				= ((1 << 4) | (1 << 12)),	/* FXO, Groupstart */
ZT_SIG_FXOKS				= ((1 << 5) | (1 << 12)),	/* FXO, Kewlstart */
ZT_SIG_EM					= (1 << 6),					/* E&M */
ZT_SIG_CLEAR				= (1 << 7),
ZT_SIG_HDLCRAW				= ((1 << 8)  | ZT_SIG_CLEAR),
ZT_SIG_HDLCFCS				= ((1 << 9)  | ZT_SIG_HDLCRAW),
ZT_SIG_CAS                  = (1 << 15),
ZT_SIG_HARDHDLC				= ((1 << 19) | ZT_SIG_CLEAR),
} sama_sigtype_t;

typedef enum {
ZT_DBIT = 1,
ZT_CBIT = 2,
ZT_BBIT = 4,
ZT_ABIT = 8
} sama_cas_bit_t;

typedef enum {
/* Tone Detection */
ZT_TONEDETECT_ON = (1 << 0), /* Detect tones */
ZT_TONEDETECT_MUTE = (1 << 1) /* Mute audio in received channel */
} sama_tone_mode_t;

/* Defines */

#define		SAMA_MAX_BLOCKSIZE	8192
#define		SAMA_DEFAULT_MTU_MRU	2048

/* ioctl defines */

#define		SAMA_CODE				'J'

#define		SAMA_GET_BLOCKSIZE	_IOR  (SAMA_CODE, 1, int)					/* Get Transfer Block Size. */
#define		SAMA_SET_BLOCKSIZE	_IOW  (SAMA_CODE, 2, int)					/* Set Transfer Block Size. */
#define		SAMA_FLUSH			_IOW  (SAMA_CODE, 3, int)					/* Flush Buffer(s) and stop I/O */
#define		SAMA_SYNC				_IOW  (SAMA_CODE, 4, int)					/* Wait for Write to Finish */
#define		SAMA_GET_PARAMS		_IOR  (SAMA_CODE, 5, struct sama_params)	/* Get channel parameters */
#define		SAMA_SET_PARAMS		_IOW  (SAMA_CODE, 6, struct sama_params)	/* Set channel parameters */
#define		SAMA_HOOK				_IOW  (SAMA_CODE, 7, int)					/* Set Hookswitch Status */
#define		SAMA_GETEVENT			_IOR  (SAMA_CODE, 8, int)					/* Get Signalling Event */
#define		SAMA_IOMUX			_IOWR (SAMA_CODE, 9, int)					/* Wait for something to happen (IO Mux) */
#define		SAMA_SPANSTAT			_IOWR (SAMA_CODE, 10, struct sama_spaninfo) /* Get Span Status */
#define		SAMA_MAINT			_IOW  (SAMA_CODE, 11, struct sama_maintinfo)/* Set Maintenance Mode for a span */
#define		SAMA_GETCONF			_IOWR (SAMA_CODE, 12, struct sama_confinfo)	/* Get Conference Mode */
#define		SAMA_SETCONF			_IOWR (SAMA_CODE, 13, struct sama_confinfo)	/* Set Conference Mode */
#define		SAMA_CONFLINK			_IOW  (SAMA_CODE, 14, struct sama_confinfo)	/* Setup or Remove Conference Link */
#define		SAMA_CONFDIAG			_IOR  (SAMA_CODE, 15, int)				/* Display Conference Diagnostic Information on Console */

#define		SAMA_GETGAINS			_IOWR (SAMA_CODE, 16, struct sama_gains)	/* Get Channel audio gains */
#define		SAMA_SETGAINS			_IOWR (SAMA_CODE, 17, struct sama_gains)	/* Set Channel audio gains */
#define		SAMA_SPANCONFIG		_IOW (SAMA_CODE, 18, struct sama_lineconfig)/* Set Line (T1) Configurations and start system  */
#define		SAMA_CHANCONFIG		_IOW (SAMA_CODE, 19, struct sama_chanconfig)/* Set Channel Configuration  */
#define		SAMA_SET_BUFINFO		_IOW (SAMA_CODE, 27, struct sama_bufferinfo)/* Set buffer policy */
#define		SAMA_GET_BUFINFO		_IOR (SAMA_CODE, 28, struct sama_bufferinfo)/* Get current buffer info */
#define		SAMA_AUDIOMODE		_IOW  (SAMA_CODE, 32, int)				/* Set a clear channel into audio mode */
#define		SAMA_ECHOCANCEL		_IOW  (SAMA_CODE, 33, int)				/* Control Echo Canceller */
#define		SAMA_HDLCRAWMODE		_IOW  (SAMA_CODE, 36, int)				/* Set a clear channel into HDLC w/out FCS checking/calculation mode */
#define		SAMA_HDLCFCSMODE		_IOW  (SAMA_CODE, 37, int)				/* Set a clear channel into HDLC w/ FCS mode */

/* Specify a channel on /dev/ftdm/chan -- must be done before any other ioctl's and is only valid on /dev/ftdm/chan */
#define		SAMA_SPECIFY			_IOW  (SAMA_CODE, 38, int)

/* Temporarily set the law on a channel to SAMA_LAW_DEFAULT, SAMA_LAW_ALAW, or SAMA_LAW_MULAW. Is reset on close. */
#define		SAMA_SETLAW			_IOW  (SAMA_CODE, 39, int)

/* Temporarily set the channel to operate in linear mode when non-zero or default law if 0 */
#define		SAMA_SETLINEAR		_IOW  (SAMA_CODE, 40, int)

#define		SAMA_GETCONFMUTE		_IOR  (SAMA_CODE, 49, int)				/* Get Conference to mute mode */
#define		SAMA_ECHOTRAIN		_IOW  (SAMA_CODE, 50, int)				/* Control Echo Trainer */

/* Set/Get CAS bits */
#define SAMA_SETTXBITS _IOW (SAMA_CODE, 43, int)
#define SAMA_GETRXBITS _IOR (SAMA_CODE, 45, int)

/*
 * Enable tone detection -- implemented by low level driver
 */
#define SAMA_TONEDETECT                _IOW(SAMA_CODE, 91, int)

#endif

/* For Emacs:
 * Local Variables:
 * mode:c
 * indent-tabs-mode:t
 * tab-width:4
 * c-basic-offset:4
 * End:
 * For VIM:
 * vim:set softtabstop=4 shiftwidth=4 tabstop=4 noet:
 */
