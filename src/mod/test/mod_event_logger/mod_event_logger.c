#include <switch.h>
#include <math.h>
#include <ctype.h>

SWITCH_MODULE_LOAD_FUNCTION(mod_event_logger_load);
SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_event_logger_shutdown);
SWITCH_MODULE_DEFINITION(mod_event_logger, mod_event_logger_load, mod_event_logger_shutdown, NULL);

void event_handler(switch_event_t *event) {
//    char *cls;
//    cls = switch_event_get_header(event, "orig-event-name");
//    if (event->event_id == SWITCH_EVENT_CHANNEL_CREATE) {
//        switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Call proccess started.\n");
//    } else if (event->event_id == SWITCH_EVENT_HEARTBEAT) {
//        switch_media_bug_t *bug = ?
//        switch_core_media_bug_set_read_replace_frame(bug, frame);
//    }
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Event id is %d\n", event->event_id);
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Event original class name is %s\n", event->subclass_name);
}

SWITCH_MODULE_LOAD_FUNCTION(mod_event_logger_load)
{
	/* connect my internal structure to the blank pointer passed to me */
    *module_interface = switch_loadable_module_create_module_interface(pool, modname);

    // TODO: continue
    if (switch_event_bind(modname, SWITCH_EVENT_ALL, SWITCH_EVENT_SUBCLASS_ANY, event_handler, NULL) != SWITCH_STATUS_SUCCESS) {
        switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Couldn't bind!\n");
    }

	/* indicate that the module should continue to be loaded */
	return SWITCH_STATUS_SUCCESS;
}

SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_event_logger_shutdown)
{
    return SWITCH_STATUS_SUCCESS;
}

/* For Emacs:
 * Local Variables:
 * mode:c
 * indent-tabs-mode:t
 * tab-width:4
 * c-basic-offset:4
 * End:
 * For VIM:
 * vim:set softtabstop=4 shiftwidth=4 tabstop=4 noet:
 */
